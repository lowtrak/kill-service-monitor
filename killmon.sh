#!/usr/bin/env bash
#
# Look for a specific process id and monitor it, killing it when found.
#

# Make sure enough args are present
if [ $# -lt 1 ]
then
  printf "%b" "Error. Not enough arguments.\n" >&2
  printf "%b" "Usage: $0 <PROCESS> <SECONDS TO WAIT (optional)>\n" >&2
  exit 2
elif [ $UID -ne 0 ]
then
  printf "%b" "Script requires sudo to run properly.\n" >&2
  printf "%b" "sudo $0 $*\n" >&2
  exit 2
else

PROCESS=$1
WAIT=${2:-"180"}  #defaults to 180 seconds if second arg is not given

#TODO: Make sure WAIT is a number.

while true
  do
    PID=`pgrep $PROCESS`

    if [ "$PID" ]
    then
      echo "Process $PID found for $PROCESS. Attempting to kill it..."

      KSTAT=`sudo kill -9 $PID`
      if [ $? -ne 0 ]
      then
        printf "ERROR. Could not kill $PROCESS.\n" >&2
        exit 2
      else
        echo "$PROCESS process id $PID was succesfuly killed!"
      fi
      
      echo "Sleeping $WAIT seconds..."
    else
      echo "$PROCESS not found. Checking again in $WAIT seconds..."
    fi

    sleep $WAIT

  done

fi
